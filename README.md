# lubuntu-setup
Automated setup scripts for [LUbuntu](https://lubuntu.net/downloads/) (Desktop and Alternate (x86/x64))


# requirements
You'll need git if you don't have it already.
``` bash
sudo apt install -y git
```

# setup
You'll find an automated setup script included.
``` bash
git clone https://gitlab.com/aceldama/lubuntu-setup.git
bash lubuntu-setup/setup
```

# usage
Just run the script you want with a "linstall-" prepended. Tab-auto-completion will also work
as the scripts are symlinked to the bin directory. For example, tin install the msf framework:
``` bash
linstall-metasploit
```
